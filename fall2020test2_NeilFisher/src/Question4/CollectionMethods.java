package Question4;

import java.util.ArrayList;
import java.util.Collection;

import Question3.Planet;

public class CollectionMethods {

	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size) {
		Collection<Planet> newPlanets = new ArrayList<Planet>();

		for (Planet planet : planets) {
			if (planet.getRadius() >= size) {
				newPlanets.add(planet);
			}
		}

		// I think this is the same thing... just testing
//		for (int i = 0; i < planets.size(); i++) {
//			if (((Planet) planets).getRadius() >= size) {
//				newPlanets.add((Planet) planets);
//			}
//		}

		return newPlanets;
	}

}
