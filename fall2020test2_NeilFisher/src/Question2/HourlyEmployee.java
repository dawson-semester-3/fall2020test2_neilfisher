package Question2;

public class HourlyEmployee implements Employee {
	private double hoursWorked;
	private double hourlyPay;

	public HourlyEmployee(double hoursWorked, double hourlyPay) {
		this.hoursWorked = hoursWorked;
		this.hourlyPay = hourlyPay;
	}

	public double getHoursWorked() {
		return hoursWorked;
	}

	public double getHourlyPay() {
		return hourlyPay;
	}

	public double getWeeklyPay() {
		return hoursWorked * hourlyPay;
	}

}
