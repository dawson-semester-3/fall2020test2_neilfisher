package Question2;

public class PayrollManagement {
	public static double getTotalExpenses(Employee[] employees) {
		double weeklyExpenses = 0;

		for (Employee employee : employees) {
			weeklyExpenses += employee.getWeeklyPay();
		}
		return weeklyExpenses;
	}

	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0] = new SalariedEmployee(50000);
		employees[1] = new HourlyEmployee(40, 20);
		employees[2] = new UnionizedHourlyEmployee(32, 12, 40, 14);
		employees[3] = new SalariedEmployee(70000);
		employees[4] = new HourlyEmployee(30, 40);

		System.out.println(getTotalExpenses(employees) + "$ in total expenses.");
	}
}
