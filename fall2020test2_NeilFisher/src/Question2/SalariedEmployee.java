package Question2;

public class SalariedEmployee implements Employee {
	private double yearlySalary;

	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}

	public double getWeeklyPay() {
		return yearlySalary / 52;
	}
}
