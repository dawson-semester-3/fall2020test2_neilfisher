package Question2;

public class UnionizedHourlyEmployee extends HourlyEmployee {
	private double maxHoursPerWeek;
	private double overtimeRate;

	public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay, double maxHoursPerWeek, double overtimeRate) {
		super(hoursWorked, hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}

	public double getWeeklyPay() {
		if (super.getHoursWorked() <= maxHoursPerWeek) {
			return getHoursWorked() * getHourlyPay();
		}
		return getHoursWorked() * overtimeRate;
	}

}
